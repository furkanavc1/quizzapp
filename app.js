
// BUTTONS
const prevBtn = document.getElementById("prev-btn")
const nextBtn = document.getElementById("next-btn")
const startBtn = document.getElementById("start-btn")
const finistheexamBtn = document.getElementById("finistheexam-btn")
// DIV
const questionEl = document.getElementById("question")
const answersDiv = document.getElementById("answers")
const finalResult = document.getElementById("quiz")
const wrapperDiv = document.getElementById("wrapper")
const btnDiv = document.getElementById("btn-div")

let countQuestion = 0
let correctAnswer = 0

finistheexamBtn.style.display = "none"
prevBtn.style.visibility = "hidden"
wrapperDiv.style.display= "none"

let quizData = [
    {
        question:"2+2 ?",
        answers:["4","5","2","1"],
        correct:0,
        answer:null
    },
    {
        question:"4+4 ?",
        answers:["4","8","10","1"],
        correct:1,
        answer:null
    },
    {
        question:"5+4 ?",
        answers:["12","3","9","2"],
        correct:2,
        answer:null
    },
    {
        question:"6*2 ?",
        answers:["12","15","1","12"],
        correct:0,
        answer:null
    },
]
function startFunc(params) {
    inputquestionGenerator(quizData)
    
    startBtn.style.display="none"
    countdown()
}

function inputquestionGenerator(q) {
    questionEl.innerHTML =   q[countQuestion].question
        const divEl = document.createElement("div")
        divEl.className = "flex flex-col"
    for(let i =0; i<q[countQuestion].answers.length;i++){
        const div2El = document.createElement("div")
        div2El.className = "flex items-center space-x-2 bg-white mb-1 p-3 rounded-2xl text-black hover:bg-green-700 transition-all ease-in-out duration-500 cursor-pointer"
        const inpEl = document.createElement("input")
        inpEl.setAttribute("type", "radio")
        inpEl.setAttribute("name", "answers")
        inpEl.setAttribute("value", i)
        const labelEl = document.createElement("label")
        labelEl.setAttribute("for", inpEl.name)
        labelEl.textContent = q[countQuestion].answers[i]
        div2El.append(inpEl)
        div2El.append(labelEl)
        divEl.append(div2El)
        answersDiv.append(divEl)
       
    }
}
function gettinAnswersBack() {  
    answersDiv.children[0].children[quizData[countQuestion].answer].children[0].checked = true
}

function nextFunc() {
    animationDiv()
    gettinAnswer()
    countQuestion++
    answersDiv.innerHTML = ""
    inputquestionGenerator(quizData)
    if(countQuestion+1 === quizData.length){
        nextBtn.style.display = "none"
        finistheexamBtn.style.display= "block"
    }
    prevBtn.style.visibility = "visible"
    if(quizData[countQuestion].answer === null){
    }else{
        gettinAnswersBack()
    }
}
    
function prevFunc() {
    countQuestion--
    answersDiv.innerHTML=""
    inputquestionGenerator(quizData)
    if(quizData[countQuestion].answer === null){
    }else{
        gettinAnswersBack()
    }
    nextBtn.style.display = "block"
    finistheexamBtn.style.display= "none"
    if(countQuestion === 0){
        prevBtn.style.visibility = "hidden"

    }else{
        prevBtn.style.visibility = "visible"

    }
    animationDivprev()
    
}
function gettinAnswer() {
    let checkedInp = document.getElementsByName("answers")
    for(let i = 0; i< checkedInp.length; i++){
        if(checkedInp[i].checked){
            quizData[countQuestion].answer = (checkedInp[i].value)
        }
    }   
}

function  checkquestions() {
    for(let i = 0; i<quizData.length;i++){
        if(quizData[i].answer == quizData[i].correct){
            correctAnswer++
        }
    }
}
function finistheexamFunc() {
    gettinAnswer()
    checkquestions()
    finalResult.innerHTML = `You got ${correctAnswer}/${quizData.length} correct`
    prevBtn.style.display = "none"
    nextBtn.style.display = "none"
    questionEl.style.display = "none"
    answersDiv.style.display = "none"
    finistheexamBtn.style.display= "none"
    btnDiv.style.display="none"   
}

function countdown() {
    const counterText = document.getElementById("counter")
    let counter = 4
    setInterval(function () {
        counter--
        if(counter >= 0){
            counterText.innerHTML= counter
        }
        if(counter === 0){
            counterText.innerHTML = ""
            wrapperDiv.style.display= "block"
        }
    },1000)

}
function animationDiv() {
    document.getElementById("animate-div").animate([
        {transform: 'translateX(0px)'},
        {transform: 'translateX(500px)'},
        {transform: 'translateX(500px)'},
        {transform: 'translateX(0px)'}
    ], {
        duration: 700
    })
}
function animationDivprev() {
    document.getElementById("animate-div").animate([
        {transform: 'translateX(0px)'},
        {transform: 'translateX(-500px)'},
        {transform: 'translateX(-500px)'},
        {transform: 'translateX(0px)'}
    ], {
        duration: 700
    })
}



nextBtn.addEventListener("click",nextFunc)
prevBtn.addEventListener("click",prevFunc)
finistheexamBtn.addEventListener("click",finistheexamFunc)
startBtn.addEventListener("click",startFunc)


